﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace svg.ViewModels
{
    public class ShelfDrawingViewModel
    {
        public int Width { get; set; } //mm

        public int Height { get; set; } //mm

        public string Color { get; set; } //string lub enum, zastanowimy sie jeszcze

        public IList<ProductItemDrawingViewModel> ProductItems { get; set; }

    }


}