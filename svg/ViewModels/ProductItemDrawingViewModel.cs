﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace svg.ViewModels
{
    public class ProductItemDrawingViewModel
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public string Color { get; set; } //string lub enum, zastanowimy sie jeszcze
    }
}
