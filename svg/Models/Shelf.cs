﻿using svg.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace svg.Models
{   

    public class Shelf
    {
        public int Width { get; set; } //mm

        public int Height { get; set; } //mm

        public string Color { get; set; } //string lub enum, zastanowimy sie jeszcze

        public IList<ProductItem> ProductItems { get; set; }

    }
}
