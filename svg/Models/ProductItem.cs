﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace svg.Models
{
    public class ProductItem
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public string Color { get; set; } //string lub enum, zastanowimy sie jeszcze
    }
}
