﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using svg.Models;
using svg.ViewModels;
using SvgNet;
using SvgNet.SvgElements;
using SvgNet.SvgTypes;

namespace svg.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()        
        {           
            return View();
        }

        public IActionResult SVG()
        {   
            var result = new List<ShelfDrawingViewModel>()
            {
                new ShelfDrawingViewModel
                {
                    Color = "#292b2c", //Inverse
                    Height = 1600,
                    Width = 3900,
                    ProductItems = new List<ProductItemDrawingViewModel>()
                    {
                        new ProductItemDrawingViewModel
                        {
                            Color = "#0275d8", //Primary
                            Height = 1050,
                            Width = 1500
                        },
                        new ProductItemDrawingViewModel
                        {
                            Color = "#5cb85c", //Success
                            Height = 1050,
                            Width = 900
                        },
                        new ProductItemDrawingViewModel
                        {
                            Color = "#d9534f", //Danger
                            Height = 1400,
                            Width = 900
                        }
                    }
                },
                new ShelfDrawingViewModel
                {
                    Color = "#f0ad4e", //Warning
                    Height = 1400,
                    Width = 3300,
                    ProductItems = new List<ProductItemDrawingViewModel>() {
                        new ProductItemDrawingViewModel
                        {
                            Color = "#5bc0de", //Info
                            Height = 1250,
                            Width = 1500
                        }
                        
                      
                    }
                }, new ShelfDrawingViewModel
                {
                    Color = "#f0ad4e", //Warning
                    Height = 1400,
                    Width = 3300,
                    ProductItems = new List<ProductItemDrawingViewModel>() {
                        new ProductItemDrawingViewModel
                        {
                            Color = "#5bc0de", //Info
                            Height = 1250,
                            Width = 1500
                        },new ProductItemDrawingViewModel
                        {
                            Color = "#d9534f", //Info
                            Height = 1250,
                            Width = 1500
                        }


                    }
                }, new ShelfDrawingViewModel
                {
                    Color = "#f0ad4e", //Warning
                    Height = 1800,
                    Width = 3900,
                    ProductItems = new List<ProductItemDrawingViewModel>() {
                        new ProductItemDrawingViewModel
                        {
                            Color = "#5cb85c", //Info
                            Height = 1400,
                            Width = 2400
                        },new ProductItemDrawingViewModel
                        {
                            Color = "#f0ad4e", //Info
                            Height = 900,
                            Width = 1000
                        }


                    } }

            };          

            return View(result.ToList());
        }              


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
